package Client;

import java.util.*;
import java.io.*;
import java.net.*;

/**
 * @author weikeng a0087836m
 * Program: HttpSocketClient.java
 * Purpose: Client program to download html file and image to local folder
 */
public class HttpSocketClient {

    private String host;
    private String remoteFile;
    private Stack<String> imageTagStack;//stack for img tag parse from html file
    private Stack<String> imgUrlStack;//stack for actual url image url
    private File localFolderDir;//folder to store actual image file
    private File templocalFolderDir;//folder to store temp img file and html page
    private File localFile;//local copy of html page

    // constructor 
    public HttpSocketClient(String urlAddress, String localDirectory) {

        getHostAndFile(urlAddress);
        this.imageTagStack = new Stack<String>();
        this.imgUrlStack = new Stack<String>();

        String tempStr = "temp";
        this.templocalFolderDir = new File(tempStr);
        if (!templocalFolderDir.exists()) {
            templocalFolderDir.mkdir();
        }
        this.localFolderDir = new File(localDirectory);
        if (!localFolderDir.exists()) {
            localFolderDir.mkdir();
        }

        this.localFile = new File("" + this.templocalFolderDir.getAbsolutePath() + "\\" + remoteFile.substring(1));
    }

    /**
     * Purpose: Split URL to get host and file part
     */
    private void getHostAndFile(String urlAddress) {
        int slashPos;

        if (urlAddress.indexOf('/') == -1) {
            host = urlAddress;
            remoteFile = urlAddress;
        } else {
            slashPos = urlAddress.indexOf('/');
            host = urlAddress.substring(0, slashPos);
            slashPos = urlAddress.indexOf('/');
            remoteFile = urlAddress.substring(slashPos, urlAddress.length());
        }
    }

    /**
     * Purpose: Download the file using socket return true if successful
     */
    private boolean downloadPageToLocal() {
        try {

            //create socket to website:80
            Socket clientSocket = new Socket(host, 80);
            System.out.println("Socket opened to " + host + "\n");

            //stream to write request to server
            OutputStreamWriter toServer = new OutputStreamWriter(clientSocket.getOutputStream());
            //stream to read response from server
            BufferedReader fromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            //stream to write read response to file
            BufferedWriter writeToFile = new BufferedWriter(new FileWriter(localFile));

            //GET request to the file
            toServer.write("GET " + remoteFile + " HTTP/1.0\r\n\n");
            toServer.flush();

            //Read remote file  and write to local file
            boolean hasMore = true;
            String input = "";
            while (hasMore) {
                input = fromServer.readLine();
                if (input == null) {
                    hasMore = false;
                } else {
                    writeToFile.write(input);
                }
            }

            //close all streams and socket
            writeToFile.close();
            toServer.close();
            fromServer.close();
            clientSocket.close();
            System.out.println("File Received..");
            return true;
        } catch (IOException ex) {
            System.out.println("Error**Unable to connect to page: " + ex + "\n");
            return false;
        }
    }

    /**
     * Purpose: Search the file for image tag and put in stack first
     */
    private void scanPageForImageTag() {
        try {
            BufferedReader fromFile = new BufferedReader(new FileReader(localFile));
            String originalStr;
            String currStr;
            String cutStr = "";
            String needToProcessStr = "";
            int index = 0, index2 = 0;
            while ((originalStr = fromFile.readLine()) != null) {

                currStr = originalStr;

                inner:
                while (currStr.length() > 0) {
                    //search for img tag prefix
                    index = currStr.indexOf("<IMG");
                    index2 = currStr.indexOf("<img");

                    if (index == -1 && index2 == -1) {
                        //end the search
                        break inner;
                    } else {
                        if ((index2 == -1) || index < index2) {
                            cutStr = currStr.substring(index);
                            int indexOfCloseBracket = 0;
                            //search for img tag closing bracket
                            indexOfCloseBracket = cutStr.indexOf('>');
                            needToProcessStr = cutStr.substring(0, indexOfCloseBracket + 1);
                            //push the whole tag string into stack
                            imageTagStack.push(needToProcessStr);

                            currStr = currStr.substring(index + indexOfCloseBracket + 1);
                        } else {
                            if (index2 != -1) {
                                cutStr = currStr.substring(index2);
                                int indexOfCloseBracket = 0;
                                indexOfCloseBracket = cutStr.indexOf('>');
                                needToProcessStr = cutStr.substring(0, indexOfCloseBracket + 1);

                                imageTagStack.push(needToProcessStr);

                                currStr = currStr.substring(index + indexOfCloseBracket + 1);
                            }
                        }
                    }
                }
            }
            fromFile.close();
        } catch (IOException ex) {
        }


    }

    /**
     * Purpose: Get the image url from within url tags in the stack
     */
    private void processImageTag() {

        while (!imageTagStack.isEmpty()) {
            int indexOfSrc = 0;
            int indexOfImg = 0;

            //search the img tag string for src= && img format to get url
            if ((indexOfSrc = (imageTagStack.peek()).indexOf("src=")) != -1 || (indexOfSrc = (imageTagStack.peek()).indexOf("src=\"")) != -1 || (indexOfSrc = (imageTagStack.peek()).indexOf("SRC=\"")) != -1) {
                if ((indexOfImg = (imageTagStack.peek()).indexOf(".gif")) != -1 || (indexOfImg = (imageTagStack.peek()).indexOf(".jpg")) != -1 || (indexOfImg = (imageTagStack.peek()).indexOf(".png")) != -1) {
                    String cutStr = (imageTagStack.peek()).substring(indexOfSrc + 5, indexOfImg + 4);

                    int indexOfHttp = 0;
                    if ((indexOfHttp = cutStr.indexOf("http://")) != -1 || (indexOfHttp = cutStr.indexOf("HTTP://")) != -1) {
                        cutStr = cutStr.substring(7);
                        imgUrlStack.push(cutStr);
                    } else {
                        int indexOfSlash = cutStr.indexOf("/");
                        cutStr = cutStr.substring(indexOfSlash);
                        cutStr = "" + host + cutStr;
                        imgUrlStack.push(cutStr);
                    }
                }
            }
            imageTagStack.pop();
        }

    }

    /**
     * Purpose: Download image from url using socket to temp folder
     */
    private void downloadImageToFolder() {
        while (!imgUrlStack.isEmpty()) {

            int slashPos = 0;
            String urlAddress = imgUrlStack.peek();
            System.out.println("Downloading.." + urlAddress);
            String toFetchhost = "";
            String toFetchremoteFile = "";
            //get img host and file name, pad actual host if img use relative path
            slashPos = urlAddress.indexOf('/');
            toFetchhost = urlAddress.substring(0, slashPos);
            slashPos = urlAddress.indexOf('/');
            toFetchremoteFile = urlAddress.substring(slashPos);

            String actualFileName = "";
            slashPos = urlAddress.lastIndexOf('/');
            actualFileName = urlAddress.substring(slashPos + 1);

            File file = new File("" + templocalFolderDir.getAbsolutePath() + "\\" + actualFileName);
            try {
                Socket clientSocket = new Socket(toFetchhost, 80);
                //stream to write request
                OutputStreamWriter toServer = new OutputStreamWriter(clientSocket.getOutputStream());

                //GET request to the file
                toServer.write("GET " + toFetchremoteFile + " HTTP/1.0\r\n\n");
                toServer.flush();
                //stream to read response
                InputStream is = clientSocket.getInputStream();
                //stream to write response
                OutputStream os = new FileOutputStream(file);

                byte[] b = new byte[2048];
                int length;

                while ((length = is.read(b)) != -1) {
                    os.write(b, 0, length);
                }
                //close streams
                is.close();
                os.close();
                toServer.close();
                clientSocket.close();
                parseImageFile(file);
            } catch (Exception ex) {
                System.out.println("Error**Unable to download: " + urlAddress + "\n");
            }
            imgUrlStack.pop();
        }
    }

    /**
     * Purpose: Remove response header in img and save in user-specified folder
     */
    private void parseImageFile(File file) {
        try {
            BufferedReader fr = new BufferedReader(new FileReader(file));
            String input = "";
            boolean hasMore = true;
            boolean parseImg = true;
            while (hasMore) {
                input = fr.readLine();
                if (input == null) {
                    hasMore = false;
                } else {
                    //check for non retrievable img first
                    int indexOfContentType = 0;
                    indexOfContentType = input.indexOf("text/html");
                    if (indexOfContentType != -1) {
                        parseImg = false;
                    }
                }
            }
            fr.close();
            BufferedReader fr2 = new BufferedReader(new FileReader(file));
            input = "";
            hasMore = true;
            if (parseImg) {
                while (hasMore) {
                    input = fr2.readLine();
                    if (input == null) {
                        hasMore = false;
                    } else {
                        //get actual content length
                        int indexOfContentLength = 0;
                        indexOfContentLength = input.indexOf("Content-Length");

                        int contentLength = 0;
                        int headerLength = 0;
                        if (indexOfContentLength != -1) {

                            contentLength = (Integer.parseInt(input.substring(input.indexOf(':') + 2)));
                            headerLength = ((int) file.length()) - contentLength;


                            File actualfile = new File("" + localFolderDir.getAbsolutePath() + "\\" + file.getName());

                            try {
                                byte[] headerbuf = new byte[headerLength];
                                InputStream in = new FileInputStream(file);
                                OutputStream out = new FileOutputStream(actualfile);
                                //read out the response header first
                                in.read(headerbuf);
                                //read n write the actual img binary
                                byte[] buf = new byte[1024];
                                int len;
                                while ((len = in.read(buf)) > 0) {
                                    out.write(buf, 0, len);
                                }

                                in.close();
                                out.close();
                                fr2.close();
                            } catch (Exception ex) {
                                //System.out.println("Error: " + ex);
                            }
                        }
                    }
                }

            }

        } catch (IOException ex) {
            System.out.println("Error: " + ex);
        }
    }

    /**
     * Purpose: Run point for program
     */
    public void startDownload() {
        if (downloadPageToLocal()) {
            System.out.println("Page downloaded to temp folder.\n");
            System.out.println("Scanning page for image tags.\n");
            scanPageForImageTag();
            System.out.println("Downloading page to local folder.\n");
            processImageTag();
            downloadImageToFolder();
        }
    }
}
