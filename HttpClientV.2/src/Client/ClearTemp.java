package Client;

import java.io.*;

/**
 * @author weikeng a0087836m
 * Program: ClearTemp.java
 * Purpose: To clear and delete temp folder
 */
public class ClearTemp {

    /**
     * Purpose: To recursively delete directory files then folder
     */
    public static boolean deleteTempFolder(File dir) {
        if (dir.isDirectory()) {
            String[] childFile = dir.list();
            for (int i = 0; i < childFile.length; i++) {
                boolean success = deleteTempFolder(new File(dir, childFile[i]));
                if (!success) {
                    return false;
                }
            }
        }
        return dir.delete();
    }

    /**
     * Purpose: Run point for ClearTemp
     */
    public void clear() {
        String localImgFolderPath = "temp";
        File templocalFolderDir;
        templocalFolderDir = new File(localImgFolderPath);
        if (!templocalFolderDir.exists()) {
            templocalFolderDir.mkdir();
        }
        deleteTempFolder(templocalFolderDir);
        System.out.println("Temp folder Cleared!");
    }
}
