Netbeans project
================
A. Package Client
================
A1. Main Class: FileDownloadGUI.java
	A1.1. Contains the GUI form for run
	A1.2. Browse for folder to save in
	A2.3. Input valid URL and click Search to retrieve images
A2. HttpSocketClient.java:
	A2.1. Download html file to temp file 
	A2.2. Scan html page for img tags and parse the tags to get img src url
	A2.3.  save the url file to temp folder
	A2.4.	Parse the url file to remove the response header in file, save the remaining file to the correct folder indicated.
A3. NewJFrame.java:  
	A3.1. Get the list of files in the directory indicated
	A3.2. Retrieve the image files and parse as image icons and display them.