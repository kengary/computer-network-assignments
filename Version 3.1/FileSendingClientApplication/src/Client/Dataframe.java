package Client;


import java.io.*;

/**
 * @author weikeng
 * Program: Dataframe.java
 * Purpose: File data and ACK Frame
 */
public class Dataframe {

    static public int ACK_MAX_SIZE = 4;
    static public int FILE_MAX_SIZE = 1500;
    static public int MAX_DATA_CONTENT_SIZE = (FILE_MAX_SIZE - 8);
    private byte[] dataContent; // file contents
    private int seqNum;
    private int hasMore;

    //FILE FRAME Constructor: Sender to create new Dataframe with Seqnum
    public Dataframe(int seqNum,int hasMore, byte[] dataContent, int len) {
        this.dataContent = new byte[len];
        System.arraycopy(dataContent, 0, this.dataContent, 0, len);
        this.seqNum = seqNum;
        this.hasMore = hasMore;
    }

    //ACK FRAME Constructor: Sender to unpack ack frame
    public Dataframe(byte[] frame) {
        ByteArrayInputStream bin = new ByteArrayInputStream(frame);
        DataInputStream dis = new DataInputStream(bin);
        try {
            seqNum = dis.readInt();
        } catch (IOException ex) {
            seqNum = -1;
            System.err.print("Error: Unable to parse Ack.");
        }
    }

    //FILE FRAME Constructor: Receiver to unpack recieved data frame
    public Dataframe(byte[] dataContent, int len) {
        ByteArrayInputStream bin = new ByteArrayInputStream(dataContent);
        DataInputStream dis = new DataInputStream(bin);
        try {
            this.seqNum = dis.readInt();
            this.hasMore = dis.readInt();
            this.dataContent = new byte[len - 8];
            dis.read(this.dataContent, 0, (len - 8));
        } catch (IOException ex) {
            System.out.println("Error: Unable to parse Data Frame.");
        }
    }

    //ACK FRAME Constructor: Receiver to create new ACK Frame 
    public Dataframe(int seqNum) {
        this.seqNum = seqNum;
    }

    //FILE FRAME Encapsulate SeqNum,hasMore,dataContent for sending
    public byte[] toDataByteArray() {
        ByteArrayOutputStream bout = new ByteArrayOutputStream(8 + dataContent.length);
        DataOutputStream dos = new DataOutputStream(bout);
        try {
            dos.writeInt(seqNum);
            dos.writeInt(hasMore);
            dos.write(dataContent);
        } catch (IOException ex) {
            System.out.println("Erorr: Unable to create Data Frame.");
        }
        return bout.toByteArray();
    }

    //ACK FRAME Encapsulate SeqNum for sending
    public byte[] toAckByteArray() {
        // create output stream for data to be written as byte array
        ByteArrayOutputStream bout = new ByteArrayOutputStream(4);
        // write primitive datatype to bout 
        DataOutputStream dos = new DataOutputStream(bout);
        try {
            dos.writeInt(seqNum);
        } catch (IOException ex) {
            System.err.println("Error: Unable to create Ack.");
        }
        return bout.toByteArray();
    }

    public int getSeqNum() {
        return this.seqNum;
    }

    public int hasMore() {
        return hasMore;
    }

    public byte[] getDataContents() {
        return dataContent;
    }
}
