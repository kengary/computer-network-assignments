package Client;

import java.net.*;
import java.io.*;
import java.util.*;

/**
 * @author weikeng
 * Program: FileSender.java
 * Purpose: Client Program to send file
 */
public class FileSender {

    private InetAddress addr;
    private int port;
    private File file;
    private int fileSize;
    private boolean connected;
    private DatagramSocket clientSocket;

    public FileSender(InetAddress addr, int port, File file) {
        this.addr = addr;
        this.port = port;
        this.file = file;
        this.fileSize = (int) file.length();
        this.connected = false;
              
    }

    // Initiate socket connection to Server
    public void run() {
        try {
            clientSocket = new DatagramSocket();
            //initConnection(clientSocket)
            if (requestForConnection()) {
                System.out.println("Connected to Server.");
                sendFileTask(clientSocket, file);
            }
        } catch (Exception ex) {
            System.out.println(ex + " Error: Unable to create client socket connection.");
        }
    }

    public boolean requestForConnection() {

        int seqNum = 0;
        String fileText = file.getAbsolutePath();
        int pos = fileText.lastIndexOf("\\") + 1;
        String fileName = fileText.substring(pos) + ":" + file.length();
        int numOfTries = 10;


        while (!connected) {
            try {
                byte[] synContent = fileName.getBytes("UTF-8");
                Dataframe synFrame = new Dataframe(seqNum,1,synContent,synContent.length);
                byte[] out=synFrame.toDataByteArray();
                DatagramPacket synPacket = new DatagramPacket(out,out.length, addr, port);
                System.out.println("Sending out syn packet..");
                clientSocket.send(synPacket);
                connected = receiveConnectionStatus(seqNum, clientSocket);
                --numOfTries;
                if (numOfTries == 0) {
                    return false;
                }
            } catch (IOException ex) {
                ex.printStackTrace();
                //return false;
            }
        }
        return true;
    }

    private boolean receiveConnectionStatus(int seqNum, DatagramSocket clientSocket) {

        DatagramPacket ackPacket = new DatagramPacket(new byte[Dataframe.ACK_MAX_SIZE], Dataframe.ACK_MAX_SIZE);

        try {
            System.out.println("Receiving ack packet..");
            clientSocket.receive(ackPacket);
            Dataframe ack = new Dataframe(ackPacket.getData());
            if ((ack.getSeqNum() - seqNum) == 1) {
                return true;
            } else {
                return false;
            }
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }

    public void sendFileTask(DatagramSocket clientSocket, File file) {
        FileInputStream fr;
        BufferedOutputStream outGoingStream;
        FileSenderProtocol protocolHandler = new FileSenderProtocol(clientSocket, addr, port, (int) file.length());
        try {
            fr = new FileInputStream(file);
            outGoingStream = new BufferedOutputStream(protocolHandler.getOutputStream());

            while (fileSize > 0) {
                outGoingStream.write(fr.read());
                outGoingStream.flush();
                fileSize--;
            }

            outGoingStream.close();
            fr.close();
            System.out.println("======================================Finished Reading File.");
        } catch (Exception ex) {
            System.out.println("Error: Unable to create output stream using protocol.");
        }
    }
}
