package Client;

import java.net.*;
import java.io.*;
import java.util.*;
/**
 * @author weikeng
 * Program: FileSenderProtocol.java
 * Purpose: Stop And Wait Protocol to for reliable UDP sends. 
 */
public class FileSenderProtocol {

    private DatagramSocket clientSocket;
    private InetAddress addr;
    private int port;
    private LinkedList<Dataframe> windowBuffer;
    private int seqNum;
    private int ackNum;
    private boolean ackFlag;
    private int bytesCount;
    private int fileSize;
    private PipedInputStream intInputStream = null;
    private PipedOutputStream uppOutputStream = null;
    private FileSenderProtocol state;
    private AckEvent ackEvent;
    private GetDataEvent getDataEvent;
    private Timer timeOutEvent;
    private int maxTimeOutLength;

    /**
     * Constructor:
     *
     */
    public FileSenderProtocol(DatagramSocket clientSocket, InetAddress addr, int port,int fileSize) {
        this.clientSocket = clientSocket;
        this.addr = addr;
        this.port = port;

        windowBuffer = new LinkedList<Dataframe>();
        this.ackNum = 0;
        this.seqNum = 0;
        this.ackFlag = false;
        this.bytesCount = 0;
        this.fileSize = fileSize;
        try {
            //Creates a piped input stream to be connect with PipedOutputStream.
            this.intInputStream = new PipedInputStream();
            //Creates a piped output stream connected to the PipedInputStream.
            this.uppOutputStream = new PipedOutputStream(intInputStream);
            //DATA BYTES written from UPPER LAYER to uppOutputStream can be read as input from intInputStream HERE
        } catch (IOException ex) {
            System.out.println("Error: Unable to create piped stream.");
        }
        this.windowBuffer = new LinkedList<Dataframe>();
        //the current state
        this.state = this;
        //Create the thread based on protocol send,stop,wait
        this.getDataEvent = new GetDataEvent();
        this.ackEvent = new AckEvent();
        this.timeOutEvent = new Timer(true);
        this.maxTimeOutLength = 1000;

        (new Thread(ackEvent)).start();
        (new Thread(getDataEvent)).start();


    }
    // Get the output stream for file data to be written to

    public OutputStream getOutputStream() {
        return uppOutputStream;
    }

    public void resetNum() {
        windowBuffer = new LinkedList<Dataframe>();
        this.ackNum = 0;
        this.seqNum = 0;
        this.bytesCount = 0;
        this.ackFlag = false;
        this.windowBuffer.removeAll(windowBuffer);
    }
    //Get data from stream and pack them into frames to be send

    private class GetDataEvent implements Runnable {

        public void run() {
            try {
                while (((fileSize - bytesCount) > 0)) {
                    System.out.println("left with : " + (fileSize - bytesCount));
                        byte[] data = new byte[Dataframe.MAX_DATA_CONTENT_SIZE];
                        int bytesRead = intInputStream.read(data);
                        if (bytesRead != -1) {
                            bytesCount += bytesRead;
                            System.out.println("bytes read : " + bytesCount);
                            synchronized (state) {
                                Dataframe packet = new Dataframe(seqNum,0,data, bytesRead);
                                seqNum++;
                                windowBuffer.add(packet);
                                if (ackNum == packet.getSeqNum()) {
                                    send(packet);
                                }
                            }
                        }
                }

            } catch (Exception e) {
                System.out.println("Error: Unable to get data from upper layer:1." + e);
            }
        }
    }

    //pack frame into UDP packets then send
    private synchronized void send(Dataframe frame) {
        // Send a datagram with the data packet.
        byte[] content = frame.toDataByteArray();
        DatagramPacket dp = new DatagramPacket(content, content.length, addr, port);
        try {
            // send UDP packet
            String s = new String(frame.getDataContents());
            System.out.println("Going to send : " + s);
            clientSocket.send(dp);
            System.out.println("Sent frame number : "+frame.getSeqNum());
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Schedule PacketTimeout timer task associated to this packet
        timeOutEvent.schedule(new FrameTimer(frame.getSeqNum()), maxTimeOutLength);
    }

    private class FrameTimer extends TimerTask {

        int currSeqNum;

        public FrameTimer(int seqNum) {
            this.currSeqNum = seqNum;
        }

        public void run() {
            synchronized (state) {
                if (ackNum == currSeqNum) {
                    this.cancel();
                    //get prev frame and resend it
                     if (!windowBuffer.isEmpty()) {
                         send(windowBuffer.getFirst());
                     }
                    System.out.println("FSP: Resending Frame number: " + currSeqNum);
                }
            }
        }
    }

    private class AckEvent implements Runnable {

        public void run() {
            try {
                while (true) {
                    DatagramPacket dp = new DatagramPacket(new byte[Dataframe.ACK_MAX_SIZE], Dataframe.ACK_MAX_SIZE);
                    clientSocket.receive(dp);

                    Dataframe ack = new Dataframe(dp.getData());
                    System.out.println("FSP: Listening Ack number: " + (ackNum));
                    System.out.println("FSP: Recieved Ack number: " + ack.getSeqNum());

                    synchronized (state) {
                        if (ackNum == ack.getSeqNum()) {
                            ackFlag = true;
                            ackNum++;
                            if (!windowBuffer.isEmpty()){
                                windowBuffer.remove();
                            }
                            if (!windowBuffer.isEmpty()) { 
                                send(windowBuffer.getFirst());
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public boolean getAckFlag() {
        return this.ackFlag;
    }
}
