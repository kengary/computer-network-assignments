package Server;

import java.net.*;
import java.io.*;
import java.util.*;
/**
 * @author weikeng
 * Program: FileRecieverProtocol.java
 * Purpose: Stop And Wait Protocol to for reliable UDP receives. 
 */
public class FileReceiverProtocol implements Runnable {

    private DatagramSocket serverSocket;
    private PipedOutputStream intOutputStream;
    private PipedInputStream uppInputStream;
    private int Rn;
    private boolean listenFlag;

    public FileReceiverProtocol(DatagramSocket serverSocket, int port) {
       
        this.Rn = 0;
        this.listenFlag=true;

        try {
             this.serverSocket =serverSocket;
            //Creates a piped output stream to be connect with PipedInputStream.
            this.intOutputStream = new PipedOutputStream();
            //Creates a piped input stream connected to the PipedOutputStream.
            this.uppInputStream = new PipedInputStream(intOutputStream);
            //DATA BYTES read from UPPER LAYER to uppInputStream can be written as output to intOutputStream HERE
        } catch (IOException ex) {
            System.out.println("Error: Unable to create piped stream.");
        }
        //(new Thread(ackEvent)).start();
        (new Thread(this)).start();
    }

    public InputStream getInputStream() {
        return uppInputStream;
    }
    public void resetNum(){
    this.Rn=0;
    this.listenFlag=true;
    }
    public void setlistenFlag(){
    this.listenFlag=false;
    }
        public void run() {
            try {
                while (listenFlag) {
                    DatagramPacket dp = new DatagramPacket(new byte[Dataframe.FILE_MAX_SIZE], Dataframe.FILE_MAX_SIZE);
                    Dataframe frame;
                    
                    System.out.println("Before receive data");
                    serverSocket.receive(dp);
                    
                    frame = new Dataframe(dp.getData(), dp.getLength());
                       String s=new String(frame.getDataContents());
                    System.out.println("After receive data: "+ s);
                    if (frame.getSeqNum() == Rn) {
                        System.out.println("Before Write data to outputstream to FS");
                        intOutputStream.write(frame.getDataContents());
                        
                        System.out.println("After Write data to outputstream to FS");
                        Rn++;
                    }

                    Dataframe ackFrame = new Dataframe(frame.getSeqNum());
                    byte[] ackArr = ackFrame.toAckByteArray();
                    DatagramPacket ack = new DatagramPacket(ackArr, ackArr.length, dp.getAddress(), dp.getPort());
                    serverSocket.send(ack);
                    System.out.println("Serversocket send ack packet");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    
}
