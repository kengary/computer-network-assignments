package Server;

import java.net.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 * @author weikeng
 * Program: FileReceiver.java
 * Purpose: Server Program to receive file
 */
public class FileReceiver extends Thread {

    protected int port;
    protected File file;
    protected int fileSize;
    private boolean connected;
    protected JTextArea jta;
    private int sizeCount;

    public FileReceiver(int port, JTextArea jta) {
        this.port = port;
        this.file = null;
        this.fileSize = 0;
        this.sizeCount = 0;
        this.connected = false;
        this.jta = jta;
    }

    public void run() {
        try {
            DatagramSocket serverSocket = new DatagramSocket(this.port);
            while (true) {
                listenToConnection(serverSocket);
                if (connected) {
                    System.out.println("Receiving file contents.");
                    fileReceiveTask(serverSocket, file);
                    System.out.println("File received.");
                    connected = false;
                    this.sizeCount = 0;
                }
            }
        } catch (IOException ex) {
            System.out.println("Error: Unable to create server socket connection");
        }
    }

    private void listenToConnection(DatagramSocket serverSocket) {
        DatagramPacket synPacket = new DatagramPacket(new byte[Dataframe.FILE_MAX_SIZE], Dataframe.FILE_MAX_SIZE);
        int ackNum = 0;

        while (!connected) {
            try {

                System.out.println("Listening for syn packet..");
                serverSocket.receive(synPacket);

                Dataframe synframe = new Dataframe(synPacket.getData(), synPacket.getLength());
                int seqNum = synframe.getSeqNum();

                if (seqNum == ackNum) {
                    ackNum++;

                    String fileInfo = new String(synframe.getDataContents(), "UTF-8");
                    System.out.println("fileInfo: " + fileInfo);
                    String fileName = fileInfo.split(":")[0];
                    System.out.println("filename: " + fileName);
                    System.out.println("fileInfo2: " + fileInfo);
                    fileSize = Integer.parseInt((fileInfo.split(":")[1]));
                    fileInfo.indexOf("");

                    this.file = new File(fileName);
                    file.createNewFile();
                    System.out.println("File created.");

                    connected = true;    
                }
                Dataframe ackframe = new Dataframe(ackNum);
                DatagramPacket ack = new DatagramPacket(ackframe.toAckByteArray(), (ackframe.toAckByteArray()).length, synPacket.getAddress(), synPacket.getPort());
                System.out.println("Sending out ack");
                serverSocket.send(ack);
                return;

            } catch (IOException ex) {
                ex.printStackTrace();
                connected = false;
                return;
            }
        }
    }

    public void fileReceiveTask(DatagramSocket serverSocket, File file) {

        try {
            FileOutputStream fr = new FileOutputStream(file);
            FileReceiverProtocol protocolHandler = new FileReceiverProtocol(serverSocket, port);
            BufferedInputStream inComingStream = new BufferedInputStream(protocolHandler.getInputStream());

            while (sizeCount < fileSize) {
                fr.write(inComingStream.read());
                sizeCount++;
            }
            inComingStream.close();
            fr.close();
            jta.append("File Recieved.\n");
            System.out.println("=================================================================File Recieved.\n");
        } catch (Exception ex) {
            System.out.println("Error: Unable to create data stream");
        }
    }
}
